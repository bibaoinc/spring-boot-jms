package com.bibao.boot.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.bibao.boot.model.Person;
import com.bibao.boot.sender.PersonSender;

@Component
@Order(1)
public class PersonRunner implements ApplicationRunner {
	@Autowired
	private PersonSender personSender;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		Person person = new Person();
		person.setFirstName("Alex");
		person.setLastName("Lee");
		person.setGender("Male");
		person.setAge(30);
		personSender.send(person);
	}

}
