package com.bibao.boot.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.bibao.boot.sender.IntegerSender;

@Component
@Order(2)
public class IntegerRunner implements CommandLineRunner {
	@Autowired
	private IntegerSender sender;
	
	@Override
	public void run(String... args) throws Exception {
		for (int i=10; i<50; i+=10) {
			sender.send(new Integer(i));
		}
	}

}
