package com.bibao.boot.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class IntegerSender implements Sender {
	private static final Logger LOG = LoggerFactory.getLogger(IntegerSender.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Override
	public void send(Object message) {
		if (message instanceof Integer) {
			Integer x = (Integer)message;
			jmsTemplate.convertAndSend("mathQueue", x);
			LOG.debug("Sending integer {}", x);
		} else {
			LOG.error("Message {} is NOT an integer", message);
		}
	}

}
