package com.bibao.boot.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.bibao.boot.model.Person;

@Component
public class PersonSender implements Sender {
	private static final Logger LOG = LoggerFactory.getLogger(PersonSender.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Override
	public void send(Object message) {
		if (message instanceof Person) {
			Person person = (Person)message;
			jmsTemplate.convertAndSend("personQueue", person);
			LOG.debug("Sending person: {}", person);
		} else {
			LOG.error("Input message {} is NOT a person", message);
		}
	}

}
