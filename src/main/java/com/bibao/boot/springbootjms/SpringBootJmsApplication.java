package com.bibao.boot.springbootjms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.bibao.boot")
public class SpringBootJmsApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootJmsApplication.class, args);
	}
	
}
