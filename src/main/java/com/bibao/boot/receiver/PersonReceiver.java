package com.bibao.boot.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.bibao.boot.model.Person;

@Component
public class PersonReceiver {
	private static final Logger LOG = LoggerFactory.getLogger(PersonReceiver.class);

	@JmsListener(destination = "personQueue", containerFactory = "jmsFactory")
	public void receive(Person person) {
		LOG.debug("Received: {}", person);
	}

}
