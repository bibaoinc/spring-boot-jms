package com.bibao.boot.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class IntegerReceiver {
	private static final Logger LOG = LoggerFactory.getLogger(PersonReceiver.class);

	@JmsListener(destination = "mathQueue", containerFactory = "jmsFactory")
	public void receive(Integer x) {
		LOG.debug("Receiving: {}", x);
		delay();
		LOG.debug("Square: " + square(x));
	}
	
	private int square(int x) {
		return x * x;
	}
	
	private void delay() {
		try {
			Thread.sleep(200);
		} catch (Exception e) {}
	}
}
